# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="bureau"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

# User configuration

# export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

export EDITOR=vim

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Aliases

alias zshconfig="vim ~/.zshrc"
alias ohmyzsh="vim ~/.oh-my-zsh"

# TTY

alias why="stty -ixon -ixoff"

# Custom programs

if [[ -d .tmuxifier ]]; then
    export PATH="$HOME/.tmuxifier/bin:$PATH"
    export TMUXIFIER_LAYOUT_PATH="$HOME/.tmuxifier-layouts"
    eval "$(tmuxifier init -)"
fi

if [[ -d $HOME/.z/ ]]; then
    export _Z_DATA="$HOME/.z_data"
    source $HOME/.z/z.sh
fi

#if [[ -f /usr/bin/virtualenvwrapper.sh ]]; then
#    source /usr/bin/virtualenvwrapper.sh
#fi

#if [[ -f /usr/local/bin/virtualenvwrapper.sh ]]; then
#    source /usr/local/bin/virtualenvwrapper.sh
#fi

if [[ -n "$VIRTUAL_ENV" ]]; then
    . $VIRTUAL_ENV/bin/preactivate
    . $VIRTUAL_ENV/bin/activate
    . $VIRTUAL_ENV/bin/postactivate
fi

if [[ -d /opt/cuda/lib64 ]]; then
    export LD_LIBARY_PATH="/opt/cuda/lib64:$LD_LIBARY_PATH"
fi

if [[ -d ~/Projects/dircolors-solarized ]]; then
    eval `dircolors ~/Projects/dircolors-solarized/dircolors.ansi-universal`
fi

export NVM_DIR="/home/mario/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

VIRTUALENVWRAPPER_PYTHON='/usr/local/bin/python3.8' # This needs to be placed before the virtualenvwrapper command
source /Library/Frameworks/Python.framework/Versions/3.8/bin/virtualenvwrapper.sh

export PATH=$PATH:/usr/local/mysql/bin
export PATH=/Library/PostgreSQL/9.5/bin:$PATH
export LANG=es_ES.UTF-8
export LC_CTYPE=es_ES.UTF-8
export LC_ALL=es_ES.UTF-8
#export DOCKER_TLS_VERIFY="1"
#export DOCKER_HOST="tcp://192.168.99.100:2376"
#export DOCKER_CERT_PATH="/Users/mario/.docker/machine/machines/default"
#export DOCKER_MACHINE_NAME="default"
#export DYLD_LIBRARY_PATH=/usr/local/lib

### Added by the Bluemix CLI
# source /usr/local/Bluemix/bx/zsh_autocomplete

export NVM_DIR="/home/mario/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
# export PATH="/usr/local/opt/openjdk/bin:$PATH"
export JAVA_HOME='/Library/Java/JavaVirtualMachines/jdk1.8.0_241.jdk/Contents/Home'
export GOPATH='/Users/marioreyes/Documents/go_dev'
export PATH=$PATH:$GOPATH/bin

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/marioreyes/Documents/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/marioreyes/Documents/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/marioreyes/Documents/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/marioreyes/Documents/google-cloud-sdk/completion.zsh.inc'; fi

export PATH="/Users/marioreyes/.pyenv/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
export PATH="/usr/local/opt/ruby/bin:$PATH"

export bi_pipelines=/Users/marioreyes/Documents/development/Heinsohh/BrightInsight/BI_DE/Pipelines
export airflow_pipelines=/Users/marioreyes/Documents/development/Heinsohh/BrightInsight/BI_DE/Pipelines/repositories/data-pipelines-airflow
export PATH="$HOME/.jenv/bin:$PATH"
eval "$(jenv init -)"
